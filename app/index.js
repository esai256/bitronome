import { vibration } from "haptics";
import document from "document";
import accurateInterval from 'accurate-interval';

const btnBR = document.getElementById("btn-br");
const btnTR = document.getElementById("btn-tr");
const btnBL = document.getElementById("btn-bl");
const btnTL = document.getElementById("btn-tl");
const tumbler = document.getElementById("tumbler");

const MINUTE = 60000;
const VIBRATION_DURATION = 100;

let interval = null;

function start() {
  interval = accurateInterval(() => {
    vibration.start("bump");
    setTimeout(() => vibration.stop(), VIBRATION_DURATION);
  }, MINUTE / getSelectedValue(), { immediate: true });
}

function stop() {
  if (interval !== null && typeof interval.clear === 'function') {
    interval.clear();
    interval = null;
  }
}

function setTo100() {
  setValue(40);
}

function setValue(value) {
  tumbler.value = value;
}

function addValue(value) {
  tumbler.value = tumbler.value += value;
}

function substractValue(value) {
  addValue(value * -1);
}

function getSelectedValue() {
  let selectedIndex = tumbler.value;
  let selectedItem = tumbler.getElementById("item" + selectedIndex);

  return selectedItem.getElementById("content").text;
}

btnTL.onactivate = function () {
  substractValue(10);
};

btnTR.onactivate = function () {
  setTo100();
};

btnBL.onactivate = function () {
  addValue(10);
};

btnBR.onactivate = function () {
  const hasStarted = interval !== null;
  const icon = btnBR.getElementById("combo-button-icon");
  const icon_press = btnBR.getElementById("combo-button-icon-press");

  if (hasStarted) {
    stop();
    icon.image = "play.png";
    icon_press.image = "play.png";
  } else {
    start();
    icon.image = "stop.png";
    icon_press.image = "stop.png";
  }
};



setValue(40);
