# Bitronome

![screenshot](Screenshot.png)

A metronome for your Fitbit Versa/Versa Lite/Ionic that vibrates in the desired rhythm. 

## Usage

-   Choose the desired speed with the tumbler-selection
-   Adjust the speed faster with +10/-10 and the reset to 100 buttons
-   Start the rhythm with the play button
-   Stop it with the sop button

## Development

Note: You need a fitbit account and nodejs.

1. Clone this repo
2. `npm install`
3. `npx fitbit`
4. Connect your device to the developer bridge (settings > 2nd from the bottom)
5. `build-and-install`
6. Have fun :tada: